picprog (1.9.1-5) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Use versioned copyright format URI.
  * Bump debhelper from old 11 to 12.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 04 Feb 2021 01:59:47 +0000

picprog (1.9.1-4) unstable; urgency=medium

  * QA upload.
  * Apply "wrap-and-sort -abst".
  * debian/control:
    + Set package maintainer to Debian QA group.
    + Bump debhelper compat to v11.
    + Bump Standards-Version to 4.2.1.
    + Add Vcs-* fields and point it to Salsa Debian git repo.
  * debian/rules:
    + Enable full hardening.
    + Use "dh_missing --fail-missing".
  * debian/copyright: Use secure uri.
  * debian/changelog: Remove trailing spaces.

 -- Boyuan Yang <byang@debian.org>  Wed, 07 Nov 2018 15:43:18 -0500

picprog (1.9.1-3) unstable; urgency=low

  * debian/control
   - bump Standards-Version to 3.9.5
  * debian/compat
   - bump to 9
  * debian/patches/30_remove-google-adsense.patch
   - remove Google AdSense
  * debian/changelog
   - fix wrong datetime

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Sun, 26 Jan 2014 19:19:14 +0900

picprog (1.9.1-2) unstable; urgency=low

  * debian/control
    - changed priority from extra to optional
    - narrowed environments: linux-any kfreebsd-any

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Sat, 3 Dec 2011 10:08:38 +0900

picprog (1.9.1-1) unstable; urgency=low

  * New upstream release
    - Fixed a bug that prevented erasing chip with OSCCAL words
    - Read also short hexfile data lines
    - Charge capacitors a little longer
    - Added some NOP commands to PIC18F programming sequence
    - Fixed warnings with current compilers
    - Changed license to GPL3
  * debian/control
    - removed dependencies: dpatch, debhelper (>> 4.0.0)
    - added dependency: debhelper (>= 8.0.0)
    - changed maintainer to Koichi Akabe (Closes: #650283)
  * debian/patches
    - removed: main.cc_include_cstdlib, strip
    - added: fix-makefile
  * debian/watch
    - added to track upstream
  * debian/rules
    - rewrote for debhelper 8.0.0
  * debian/copyright
    - changed format to dep5

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Tue, 29 Nov 2011 18:33:50 +0900

picprog (1.9.0-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "non-standard gcc/g++ used for build (gcc-4.3)" and
    "FTBFS: build-dependency not installable: libstdc++6-4.3-dev":
    drop build dependency on libstdc++6-4.3-dev
    Closes: #629741, #594293
    LP: #745544

 -- gregor herrmann <gregoa@debian.org>  Thu, 15 Sep 2011 17:45:14 +0200

picprog (1.9.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Avoiding iopl() on platforms that don't have it. (closes: #492812)

 -- Jelmer Vernooij <jelmer@samba.org>  Mon, 29 Sep 2008 02:30:46 +0200

picprog (1.9.0-1) unstable; urgency=low

  * New upstream release
    - Chip presence is no more detected with start/stop bit checking
      (closes: #412778)

 -- Jan Wagemakers <janw@dommel.be>  Sat, 19 Jul 2008 10:59:54 +0200

picprog (1.8.3-2) unstable; urgency=low

  * added dpatch 10_gcc4.3.dpatch (closes: #417475)
  * added dpatch 20_strip.dpatch  (closes: #437774)

 -- Jan Wagemakers <janw@dommel.be>  Sat, 18 Aug 2007 10:42:07 +0200

picprog (1.8.3-1) unstable; urgency=low

  * New upstream release

 -- Jan Wagemakers <janw@dommel.be>  Sat, 30 Sep 2006 11:17:35 +0200

picprog (1.8.2-1) unstable; urgency=low

  * New upstream release
    - This release has two new options --nordtsc and --slow,
      (closes: #387185)
  * Changed section to "electronics".

 -- Jan Wagemakers <janw@dommel.be>  Tue, 26 Sep 2006 10:55:52 +0200

picprog (1.8.1-3) unstable; urgency=low

  * Changed picport.h so that it compiles with g++-4.1
  * Fixed "ITP: picprog -- Microchip PIC microcontroller programmer
    software", closes: #208692

 -- Jan Wagemakers <janw@dommel.be>  Sat,  8 Jul 2006 10:24:02 +0200

picprog (1.8.1-2) unstable; urgency=low

  * Corrected the homepage description in debian/control
  * package now depends on libstdc++6-4.0-dev

 -- Jan Wagemakers <janw@easynet.be>  Sun,  2 Apr 2006 17:32:12 +0200

picprog (1.8.1-1) unstable; urgency=low

  * New upstream release

 -- Jan Wagemakers <janw@easynet.be>  Sun, 26 Mar 2006 17:11:23 +0200

picprog (1.8-2) unstable; urgency=low

  * Fixed picprog.1 (man page) to make package lintian clean

 -- Jan Wagemakers <janw@easynet.be>  Sun, 12 Mar 2006 07:24:21 +0100

picprog (1.8-1) unstable; urgency=low

  * New upstream release

 -- Jan Wagemakers <janw@easynet.be>  Mon,  6 Mar 2006 16:44:05 +0100

picprog (1.7-2) unstable; urgency=low

  * Fixed error in picprog.doc-base

 -- Jan Wagemakers <janw@easynet.be>  Sun, 18 Jul 2004 18:07:05 +0200

picprog (1.7-1) unstable; urgency=low

  * New upstream release

 -- Jan Wagemakers <janw@easynet.be>  Sat,  1 May 2004 07:41:06 +0200

picprog (1.6-1) unstable; urgency=low

  * New upstream release

 -- Jan Wagemakers <janw@easynet.be>  Fri, 19 Mar 2004 16:12:02 +0100

picprog (1.5-1) unstable; urgency=low

  * New upstream release

 -- Jan Wagemakers <janw@easynet.be>  Thu,  4 Mar 2004 16:20:05 +0100

picprog (1.4-1) unstable; urgency=low

  * New upstream release

 -- Jan Wagemakers <janw@easynet.be>  Fri,  2 Jan 2004 10:18:29 +0100

picprog (1.3-4) unstable; urgency=low

  * Changed the description in the file debian/control

 -- Jan Wagemakers <janw@easynet.be>  Fri,  5 Sep 2003 11:55:20 +0200

picprog (1.3-3) unstable; urgency=low

  * Changed the description in the file debian/control

 -- Jan Wagemakers <janw@easynet.be>  Fri,  5 Sep 2003 00:37:38 +0200

picprog (1.3-2) unstable; urgency=low

  * Changed author(s) to author in the file copyright

 -- Jan Wagemakers <janw@easynet.be>  Sat, 30 Aug 2003 10:44:41 +0200

picprog (1.3-1) unstable; urgency=low

  * New upstream release

 -- Jan Wagemakers <janw@easynet.be>  Fri, 22 Aug 2003 07:50:35 +0200

picprog (1.2-1) unstable; urgency=low

  * Initial Release.

 -- Jan Wagemakers <janw@easynet.be>  Mon, 18 Aug 2003 08:30:45 +0200
